---
title: "Векторные вычисления в R: сравнение изменений"
author: "Шалаев Н. Е."
date: '14 марта 2020 г.'
output:
  pdf_document:
    latex_engine: xelatex
  html_document: default
header-includes:
  - \usepackage{fontspec}
  - \setmainfont{PT Mono}
---

Примерные данные взяты из статьи The New Political Economy of Taxation in Advanced Capitalist Democracies (D. Swank, S. Steinmo, American Journal of Political Science, Vol. 46, No. 3, July 2002, Pp. 642–655):

```{r}
cnames <- c("Australia", "Austria", "Belgium", 
		"Canada", "Denmark", "Finland", 
		"France", "Germany", "Ireland", 
		"Italy", "Japan", "Netherlands", 
		"New Zealand", "Norway", "Sweden", 
		"Switzerland", "United Kingdom", "United States") 
v1981<-c(30,44,45,35,45,38,42,38,33,31,26,44,34,49,50,31,36,29)
v1995<-c(31,42,46,37,51,46,44,39,34,41,29,44,38,46,50,34,36,28)

taxationGDP <- data.frame (row.names = cnames, year1981 = v1981, year1995 = v1995)
```

Подключим таблицу с данными, чтобы упростить адресацию столбцов (не нужно будет писать taxationGDP$...)

```{r}
attach(taxationGDP)
```

Воспользуемся формулой для нормы разности векторов, чтобы получить разницу между 1981 и 1995 годами:

```{r}
sum(year1995*year1995)-2*sum(year1995*year1981)+sum(year1981*year1981)
```

Сделаем синтетический набор данных: умножив данные по 1981 году на 1,1 мы имитируем равномерный прирост по всей совокупности на 10%:
```{r}
year1995f <- year1981*1.1
```

Сравним исходные данные и синтетические:

```{r}
sum(year1995f*year1995f)-2*sum(year1995f*year1981)+sum(year1981*year1981)
```

Насколько чувствительна наша мера? Повторим процедуру выше с отступом ±1%

```{r}
year1995f <- year1981*1.11
sum(year1995f*year1995f)-2*sum(year1995f*year1981)+sum(year1981*year1981)

year1995f <- year1981*1.09
sum(year1995f*year1995f)-2*sum(year1995f*year1981)+sum(year1981*year1981)
```

Таким образом, наша мера вполне чувствительна, и мы можем оценить масштаб изменений между 1981 и 1995 годами как соразмерный 10% приросту по совокупности. Что же говорят нам средние по годам? 

```{r}
mean(year1981)
mean(year1995)

(mean(year1995) - mean(year1981)) / mean(year1981)
```

Итак, если взять 1981 год за базу, то изучение средних подсказывает оценку прироста в 5%. Что в два раза отличается от векторной оценки.

В качестве финального аккорда проиллюстрируем графиком наши данные:

```{r}
plot(year1995, pch=19, col = 'red', xaxt = 'n', xlab = '', ylab = '')
points(year1981, pch=19, col = 'blue')
axis(at = 1:nrow(taxationGDP), labels = rownames(taxationGDP), side = 1, las=2)
```

Именно такой разброс мы и пытались охарактеризовать одним числом в этом примере.